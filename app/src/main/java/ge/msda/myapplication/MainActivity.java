package ge.msda.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Books books1 = new Books();
        books1.setBookname("yjkuuo");
        books1.setBookauthor("nireng");
        books1.setBookprice(25F);

        Books Book2 = new Books();
        Book2.setBookname("Mercedes Benz S550");
        Book2.setBookauthor("eifhueygb");
        Book2.setBookprice(45F);

        App.getInstance().getAppDatabase().getBookDao().insert(books1, Book2);

        List<Books> allBooks = App.getInstance().getAppDatabase().getBookDao().getAllBooks();
        for (int i = 0; i < allBooks.size(); i++) {
            Log.d("MyBook", allBooks.get(i).toString());
        }

    }
}